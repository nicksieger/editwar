#!/usr/bin/env ruby
# == Synopsis
# Utility used to manage J2EE archives (EAR, WAR, ...).
# This utility provides many commands that allows one to easily edit
# and change configuration setting in many kind of J2EE archives.
# 
# == Usage
# editwar [OPTION] ACTION  locator
# === -h, --help:
#   show help
# === locator:
#   The location that will be used by the specified command.
#   The format for the locator is as follow :
#     file_locator[:file_locator]
#   Each file_locator is a path within the parent filesystem used to locate the file.
#   This syntax allows to locate files within archives.
#   Examples :
#     my_application.ear:my_war.war:web.xml
#     Will locate the web.xml file contained in my_war.war archive contained in the 
#     my_application.ear enterprise archive.
#     
#     my_application.ear
#     Will locate the enterprise application archive named my_application.ear in the current
#     folder.
# == OPTION
# === -l:
#   Show line numbers when displaying files
# === -n:
#   NOT YET IMPLEMENTED. Do not change any files.
# === -e:
#   Specify which editor to run.
#   By default, the EDITOR environment variable will be used to locate the editor.
# == ACTION
# === --view, -V:
#   display the content of the file specified by ''locator''.
# === --edit, -E:
#   start an editor to edit the file specified by ''locator''. When the editor
#   exits, the file will be replaced (within it's container) by the edited file.
# === --remove [backup], --rm [backup], -R [backup], --del [backup]:
#   remove the file specified by ''locator''
#   If [backup] is specified, the file will extracted to that location before removing it.
#   If this option is the last before the locator and you don't want to save the removed file
#   you have to add an empty option (--) before the locator
#   ex:
#     editwar --remove -- my-application.ear:webapp.war
# === --add file_to_add, -A file_to_add, --update file_to_add
#   add or update the file specified by ''locator'' with ''file_to_add'' located on the filesystem
#   ''file_to_add'' muste specified a full file name within the archive and not just a folder
#   ex:
#     editwar --add some-library.jar my-application.ear:webapp.war:WEB-INF/lib/some-library.jar

require 'getoptlong'
require 'rdoc/usage'
require 'editwar'

opts = GetoptLong.new(
  ['--help', '-h', GetoptLong::NO_ARGUMENT],
  ['--view', '-V', GetoptLong::NO_ARGUMENT],
  ['--edit', '-E', GetoptLong::NO_ARGUMENT],
  ['--remove', '--rm', '-R', '--del', GetoptLong::OPTIONAL_ARGUMENT],
  ['--add', '-A', '--update', GetoptLong::REQUIRED_ARGUMENT],
  ['-e', GetoptLong::REQUIRED_ARGUMENT],
  ['-l', GetoptLong::NO_ARGUMENT]
)
locator = action = action_arg = nil
extra_opts = {}
extra_opts[:editor]= ENV['EDITOR']

begin
  opts.each do |opt, arg|
    case opt
      when '--help'
        RDoc::usage
      when '-e'
        extra_opts[:editor]= arg
      when '-l'
        extra_opts[:show_line_number]= true
      else
        if !action.nil?
          puts "Only one action can be specified"
          exit(1)
        end
        action= opt.gsub(/^--/, '')
        action_arg= arg
    end
  end
rescue GetoptLong::Error => e
  RDoc::usage(1)
end

locator = ARGV.shift if ARGV.length >= 1

RDoc::usage(1) if locator.nil? || action.nil?

locators = locator.split(':')

Editwar.new.on_last_locator(locators, nil) do |editwar, target, container|
  editwar.get_action(action).execute(target, action_arg, extra_opts, container)
end
