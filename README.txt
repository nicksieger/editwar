= editwar

* http://caldersphere.rubyforge.org/editwar

== DESCRIPTION:

editwar is a command-line utility to manage war file contents. See http://epirsch.blogspot.com/2007_01_01_archive.html for the original inspiration and code.

== FEATURES/PROBLEMS:

* Open a file embedded in the war inside your $EDITOR, and update its contents in the war when finished.
* View contents of embedded files
* Add/remove files in the war

== SYNOPSIS:

Utility used to manage J2EE archives (EAR, WAR, ...).
This utility provides many commands that allows one to easily edit
and change configuration setting in many kind of J2EE archives.

    editwar [OPTIONS] ACTION LOCATOR
      -h, --help:
        show help
    locator:
      The location that will be used by the specified command.
      The format for the locator is as follow :
        file_locator[:file_locator]
      Each file_locator is a path within the parent filesystem used to locate the file.
      This syntax allows to locate files within archives.
      Examples :
        my_application.ear:my_war.war:web.xml
        Will locate the web.xml file contained in my_war.war archive contained in the 
        my_application.ear enterprise archive.
    
        my_application.ear
        Will locate the enterprise application archive named my_application.ear in the current
        folder.
    options:
    -l:
      Show line numbers when displaying files
    -n:
      NOT YET IMPLEMENTED. Do not change any files.
    -e:
      Specify which editor to run.
      By default, the EDITOR environment variable will be used to locate the editor.
    action:
    --view, -V:
      display the content of the file specified by ''locator''.
    --edit, -E:
      start an editor to edit the file specified by ''locator''. When the editor
      exits, the file will be replaced (within it's container) by the edited file.
    --remove [backup], --rm [backup], -R [backup], --del [backup]:
      remove the file specified by ''locator''
      If [backup] is specified, the file will extracted to that location before removing it.
      If this option is the last before the locator and you don't want to save the removed file
      you have to add an empty option (--) before the locator
      ex:
        editwar --remove -- my-application.ear:webapp.war
    --add file_to_add, -A file_to_add, --update file_to_add
      add or update the file specified by ''locator'' with ''file_to_add'' located on the filesystem
      ''file_to_add'' muste specified a full file name within the archive and not just a folder
      ex:
        editwar --add some-library.jar my-application.ear:webapp.war:WEB-INF/lib/some-library.jar

== REQUIREMENTS:

* None

== INSTALL:

* gem install editwar

== LICENSE:

(The MIT License)

Copyright (c) 2008 Nick Sieger
Original code credited to Emmanuel Pirsch

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
