require 'zip/zip'

class Editwar
  VERSION = '0.1'
  include Zip

  class Action
    def initialize(editwar)
      @editwar = editwar
    end
  end

  class ViewAction < Action
    def execute(target, arg, opts, container= nil)
      @editwar.on_extracted_file(container, target) do |file|
        if @editwar.is_archive(target)
          view_archive(file)
        else
          view_file(file, opts)
        end
        false
      end
      false
    end
    private
    def view_archive(file)
      ZipFile.open(file) do |zipfile|
        names= []
        zipfile.each do |entry|
          names << entry.to_s
        end
        names.sort.each do |entry|
          puts entry
        end
      end
    end
    def view_file(file, opts)
      line_number=0
      File.new(file).each_line do |line|
        print "#{line_number+= 1}: " if opts[:show_line_number]
        puts line
      end
    end
  end

  class EditAction < Action
    def execute(target, arg, opts, container= nil)
      if @editwar.is_archive(target)
        puts "Cannot edit archives!"
        return false
      end

      @editwar.on_extracted_file(container, target) do |file|
        if (opts[:editor].nil?)
          puts "No editor specified!"
          RDoc::usage_no_exit("-e:")
          false
        else 
          `#{opts[:editor]} #{file}`
          puts 'Done editing'
          true
        end
      end
    end
  end

  class RemoveAction < Action
    def execute(target, arg, opts, container= nil)
      return false if container.nil?

      save_file(target, arg, opts, container) unless arg.nil? || arg.empty?
      container.remove(target)
      puts("#{target} removed.")
      true
    end

    private
    def save_file(target, arg, opts, container)
      filename= File.basename(target)
      container.extract(target, File.directory?(arg) ? arg+"/"+filename : arg) {true}
    end
  end

  class AddAction < Action
    def execute(target, arg, opts, container= nil)
      return false if container.nil?

      container.remove(target) if container.find_entry(target)
      container.add(target, arg)
      puts "#{arg} added as #{target}."
      true
    end
  end

  def on_extracted_file(container, target)
    if container.nil?
      return yield(target)
    else
      tempfile= Dir.tmpdir+"/"+target.gsub('/', '_')
      container.extract(target, tempfile.to_s) {true}
      if yield(tempfile)
        container.remove(target)
        container.add(target, tempfile)
        return true
      else
        return false
      end
      FileUtils.rm tempfile, :force => true
    end
  end

  def is_archive(file) 
    file =~ /(\.zip|\.ear|\.war|\.jar)$/i
  end

  def get_action(action)
    action= action.capitalize + "Action"
    eval(action).new(self)
  end

  def on_last_locator(locators, container, &action)
    locator= locators.shift
    last= locators.length == 0

    if last
      return action.call(self, locator, container)
    else
      on_extracted_file(container, locator) do |file|
        ZipFile.open(file) do |zipfile|
          on_last_locator(locators, zipfile, &action)
        end
      end
    end
  end
end
